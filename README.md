# capstone

### Running Project (no hardware)

  This project can only be run without hardware on the no_hardware branch. Without hardware the program will simply request moves and print the board.

  Install python 2.7 and install python-chess with ```pip install python-chess```. Then simply ```python src/main.py```.

#### Parts List
  rainbowduino,
  Atlas-SoC,
  64 common anode RGB LEDs,
  64 phototransistors,
  64 470 KOhm resistors,
  chessboard,
  general EE parts for wiring and soldering,
  some woodworking is required,
  some plexiglass

#### Picture of completed board with game in progress
  ![The board highlights squares that each player controls](media/IMG_4118.jpg?raw=true "Board")
  
A video of the board running can be found in the media folder.